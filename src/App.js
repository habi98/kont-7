import React, { Component } from 'react';

import Buttons from './component/MenuButtons/MenuButtons';
// import Order from './component/Order/Order'
import OrderBlock from './component/OrderBlock/OrderBlock'
import './App.css';


class App extends Component {

 state = {
   Menu : [
       {name: 'Hamburger', count: 0},
       {name: 'Cheeseburger', count: 0},
       {name: 'Hot-dog', count: 0},
       {name: 'Fries', count: 0},
       {name: 'Tea', count: 0},
       {name: 'Coffee', count: 0},
       {name: 'Pepsi', count: 0},
       {name: 'Coca-cola', count: 0},
   ]
 };

 addItems = (name) => {
     let menu = [...this.state.Menu] ;
     for(let i = 0; i < menu.length; i ++) {
         if (name === menu[i].name) {
             menu[i].count++;
         }
     }
         this.setState({
             Menu: menu
         })
 };
    removeItems = (name) => {
        let menu = [...this.state.Menu];
        for(let i = 0; i < menu.length; i ++) {
            if (name === menu[i].name && menu[i].count > 0) {
                menu[i].count--;
            }
        }
        this.setState({
            Menu: menu
        })
    };

  render() {
    return (
      <div className="App">




            <OrderBlock ingMenu={this.state.Menu} />

      <Buttons ingMenu={this.state.Menu} click={(name) => this.addItems(name)} Click={(name) => this.removeItems(name)}/>
      </div>
    );
  }
}

export default App;
