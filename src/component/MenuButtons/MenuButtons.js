import React from 'react';

import burger from '../../assets/burger.png';
import cheeseburger from '../../assets/cheeseburger.png';
import hotdog from '../../assets/hotdog.png';
import fries from '../../assets/icons8-french-fries-30.png';
import tea from '../../assets/icons8-tea-30.png';
import coffee from '../../assets/icons8-coffee-to-go-30.png'
import cola from '../../assets/iconfinder_cocacola_294680.png'
import pepsi from '../../assets/iconfinder_pepsi_294663.png'
import './MenuButtons.css';

const menu = [
    {name: 'Hamburger', price: 80 + ' kgs', img: burger},
    {name: 'Cheeseburger', price: 60 + ' kgs', img: cheeseburger},
    {name: 'Hot-dog', price: 55 + ' kgs', img: hotdog},
    {name: 'Fries', price: 40 + ' kgs', img: fries},
    {name: 'Tea', price: 20 + ' kgs', img: tea},
    {name: 'Coffee', price: 30 + ' kgs', img: coffee},
    {name: 'Pepsi', price: 25 + ' kgs', img: pepsi},
    {name: 'Coca-cola', price: 25 + ' kgs', img: cola},

];

const getCountIngredient = (props, ingredientName) => {
    let id = props.ingMenu.findIndex((Menu) => {
        return Menu.name === ingredientName
    });
    return props.ingMenu[id].count;
};

const Buttons = (props) => {
    return (
        <div>
            <p>Add items: </p>
            {menu.map((Menu, id) => {
                return (
                    <div key={id} className="btn-menu">
                        <button type="button" onClick={() => props.click(Menu.name)}><p><img src={`${Menu.img}`} alt=""/></p><p>{Menu.name}</p>{Menu.price}</button>
                        <span>{getCountIngredient(props, Menu.name)} </span><button type="button"  onClick={() => props.Click(Menu.name)} >x</button>
                    </div>
                )
            })}
        </div>
        )

};

export default Buttons;