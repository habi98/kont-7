import React from 'react';

const Order = (props) => {

    const getOrder = () => {
        let names = [];
            if(props.count > 0) {
                names.push(props.name);
        }
        return names;
    };
    return (
        getOrder().map((menu, index) => {
            return(
                <div key={index}><p>{menu}</p></div>
            )
        })
    )
};

export default Order;