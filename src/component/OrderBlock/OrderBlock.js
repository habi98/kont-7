import React from 'react';
import Order from '../Order/Order';



const OrderBlock = (props) => {
    return(
        <div>
            {props.ingMenu.map((menu, index) => {
                return(
                    <Order key={index} count={menu.count} name={menu.name} onClick={() => props.Click(menu.name)}/>
                )
            })}
        </div>
    )
};

export default OrderBlock;